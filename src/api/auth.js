/* eslint-disable no-unused-vars */
import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';

const TokenKey = 'Unifi-Token';
const salt = bcrypt.genSaltSync(10);

export function getToken() {
  return localStorage.getItem(TokenKey);
}

export function setToken(token) {
  return localStorage.setItem(TokenKey, token);
}

export function removeToken() {
  return localStorage.removeItem(TokenKey);
}

export default class Auth {
  login(userInfo) {
    return new Promise((resolve, reject) => {
      // parse stringified users in localStorage
      const users = JSON.parse(localStorage.getItem('users')) || [];
      let { username, password } = userInfo;
      username = username.trim();
  
      let usernameExists = false, hashedPassword = '';
      for (const user of users) {
        if (user.username === username) {
          hashedPassword = user.password;
          usernameExists = true;
          break;
        }
      }
  
      if (!usernameExists) {
        reject('Username doesn\'t exists');
      }
  
      const valid = bcrypt.compareSync(password, hashedPassword);
      if (!valid) {
        reject('Password is incorrect');
      }
  
      const token = jwt.sign({ data: username }, 'secret', { expiresIn: '7d' });
      resolve({
        username,
        token
      });
    });
  }
  
  signup(userInfo) {
    return new Promise((resolve, reject) => {
      let users = JSON.parse(localStorage.getItem('users')) || [];
      let { username, password } = userInfo;
      username = username.trim();

      // check if password length less than 4 characters
      if (password.length < 4) {
        reject('Password must be 4 characters at least');
      }

      // check if username is not exist
      let usernameExists = false;
      for (const user of users) {
        if (user.username === username) {
          usernameExists = true;
          break;
        }
      }
    
      if (usernameExists) {
        reject('Username is already exists');
      }
    
      const hashedPassword = bcrypt.hashSync(password, salt);
      users.push({
        username: username,
        password: hashedPassword
      });
      localStorage.setItem('users', JSON.stringify(users));
      resolve();
    });
  }
  
  logout() {
    return new Promise((resolve, reject) => {
      resolve();
    });
  }
  
}
