/* eslint-disable no-unused-vars */
import { maxInArray } from '@/utils';
export default class Cart {
  get() {
    return new Promise((resolve, reject) => {
      const cart = JSON.parse(localStorage.getItem('cart')) || [];
      resolve(cart);
    });
  }

  addIngredient(data) {
    return new Promise((resolve, reject) => {
      const cart = JSON.parse(localStorage.getItem('cart')) || [];
      // add id to ingredient which is its index in the cart array
      let id = maxInArray(cart, true, 'id');
      id = (id !== -1) ? id + 1 : 0;
      const newIngredient = {
        id,
        ...data
      };
      
      cart.push(newIngredient);
      localStorage.setItem('cart', JSON.stringify(cart));
      resolve(newIngredient);
    });
  }

  removeIngredient(id) {
    return new Promise((resolve, reject) => {
      const cart = JSON.parse(localStorage.getItem('cart')) || [];
      cart.splice(id, 1);
      localStorage.setItem('cart', JSON.stringify(cart));
      resolve();
    });
  }

  updateIngredient(id, data) {
    return new Promise((resolve, reject) => {
      const cart = JSON.parse(localStorage.getItem('cart')) || [];
      for (const ingredient of cart) {
        if (ingredient.id === id) {
          cart[id] = data;
          resolve(data);
        }
      }
      reject('Error please try again');
    });
  }

  destroy() {
    return new Promise((resolve, reject) => {
      let cart = JSON.parse(localStorage.getItem('cart')) || [];
      cart = [];
      localStorage.setItem('cart', JSON.stringify(cart));
      resolve();
    });
  }

  update(data) {
    return new Promise((resolve, reject) => {
      localStorage.setItem('cart', JSON.stringify(data));
      resolve();
    });
  }
}
