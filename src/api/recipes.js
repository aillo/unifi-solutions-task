/* eslint-disable no-unused-vars */
import { maxInArray } from '@/utils';
export default class Recipes {
  list() {
    return new Promise((resolve, reject) => {
      // instead of real api
      const recipes = JSON.parse(localStorage.getItem('recipes')) || [];
      resolve(recipes);
    });
  }

  get(id) {
    return new Promise((resolve, reject) => {
      const recipes = JSON.parse(localStorage.getItem('recipes')) || [];
      for (const recipe of recipes) {
        if (recipe.id == id) {
          resolve(recipe);
        }
      }
      reject('Recipe doesn\'t exist');
    });
  }

  store(data) {
    return new Promise((resolve, reject) => {
      const recipes = JSON.parse(localStorage.getItem('recipes')) || [];
      // add id to recipe which is its index in the recipes array
      let id = maxInArray(recipes, true, 'id');
      id = (id !== -1) ? id + 1 : 0;
      const newRecipe = {
        id,
        ...data
      };
      
      recipes.push(newRecipe);
      localStorage.setItem('recipes', JSON.stringify(recipes));
      resolve(newRecipe);
    });
  }

  destroy(id) {
    return new Promise((resolve, reject) => {
      const recipes = JSON.parse(localStorage.getItem('recipes')) || [];
      const newRecipes = recipes.filter(item => item.id != id);
      localStorage.setItem('recipes', JSON.stringify(newRecipes));
      resolve();
    });
  }

  update(id, data) {
    return new Promise((resolve, reject) => {
      const recipes = JSON.parse(localStorage.getItem('recipes')) || [];
      for (let i = 0; i < recipes.length; i++) {
        if (recipes[i].id == id) {
          recipes[i] = data;
          localStorage.setItem('recipes', JSON.stringify(recipes));
          resolve(data);
        }
      }
      reject('Error please try again');
    });
  }
}
