import Cart from '@/api/cart';

const cart = new Cart();

const state = {
  cart: []
};

const mutations = {
  SET_CART: (state, cart) => {
    state.cart = cart;
  },
  ADD_INGREDIENT: (state, ingredient) => {
    state.cart.push(ingredient);
  },
  DELETE_INGREDIENT: (state, id) => {
    if (id in state.cart) {
      state.cart.splice(id, 1);
    }
  },
  UPDATE_INGREDIENT: (state, data) => {
    state.cart[data.id] = data.ingredient;
  },
  UPDATE_CART: (state, cart) => {
    state.recipes = cart;
  }
};

const actions = {
  fetchCart({ commit }) {
    return new Promise((resolve, reject) => {
      cart.get().then(data => {
        commit('SET_CART', data);
        resolve(data);
      }).catch(err => {
        commit('SET_CART', []);
        reject(err);
      })
    });
  },
  updateCart({ commit }, data) {
    return new Promise((resolve, reject) => {
      cart.update(data).then(data => {
        commit('UPDATE_CART', data);
        resolve();
      }).catch(err => {
        reject(err);
      });
    });
  },
  addIngredient({ commit }, ingredientInfo) {
    return new Promise((resolve, reject) => {
      cart.addIngredient(ingredientInfo).then((ingredient) => {
        commit('ADD_INGREDIENT', ingredient);
        resolve();
      }).catch(err => {
        reject(err);
      });
    });
  },
  removeIngredient({ commit }, id) {
    return new Promise((resolve, reject) => {
      cart.removeIngredient(id).then(() => {
        commit('DELETE_INGREDIENT', id);
        resolve();
      }).catch(err => {
        reject(err);
      })
    });
  },
  updateIngredient({ commit }, data) {
    return new Promise((resolve, reject) => {
      cart.updateIngredient(data.id, data.ingredient).then(data => {
        commit('UPDATE_INGREDIENT', data);
        resolve();
      }).catch(err => {
        reject(err);
      });
    });
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
