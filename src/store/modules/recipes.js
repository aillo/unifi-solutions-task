import Recipes from '@/api/recipes';

const recipes = new Recipes();

const state = {
  recipes: []
};

const mutations = {
  SET_RECIPES: (state, recipes) => {
    state.recipes = recipes;
  },
  ADD_RECIPE: (state, recipe) => {
    state.recipes.push(recipe);
  },
  DELETE_RECIPE: (state, id) => {
    if (id in state.recipes) {
      state.recipes.splice(id, 1);
    }
  },
  EDIT_RECIPE: (state, recipe) => {
    state.recipes[recipe.id] = recipe;
  }
};

const actions = {
  fetchRecipes({ commit }) {
    return new Promise((resolve, reject) => {
      recipes.list().then(data => {
        commit('SET_RECIPES', data);
        resolve(data);
      }).catch(err => {
        commit('SET_RECIPES', []);
        reject(err);
      })
    })
  },
  addRecipe({ commit }, recipeInfo) {
    return new Promise((resolve, reject) => {
      recipes.store(recipeInfo).then((recipe) => {
        commit('ADD_RECIPE', recipe);
        resolve();
      }).catch(err => {
        reject(err);
      });
    });
  },
  // eslint-disable-next-line no-unused-vars
  getRecipe({commit}, id) {
    return new Promise((resolve, reject) => {
      recipes.get(id).then(recipe => {
        resolve(recipe);
      }).catch(err => {
        reject(err);
      })
    });
  },
  deleteRecipe({ commit }, id) {
    return new Promise((resolve, reject) => {
      recipes.destroy(id).then(() => {
        commit('DELETE_RECIPE', id);
        resolve();
      }).catch(err => {
        reject(err);
      })
    });
  },
  updateRecipe({ commit }, data) {
    return new Promise((resolve, reject) => {
      recipes.update(data.id, data.recipe).then(data => {
        commit('EDIT_RECIPE', data);
        resolve();
      }).catch(err => {
        reject(err);
      });
    });
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
