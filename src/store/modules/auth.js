import Auth, { getToken, setToken, removeToken } from "@/api/auth";
import { toast } from '@/utils/toast';
import router from '@/router';

const auth = new Auth();

const state = {
  isLoggedIn: !!getToken()
};

const mutations = {
  LOGIN_USER: state => state.isLoggedIn = true,
  LOGOUT_USER: state =>state.isLoggedIn = false
};

const actions = {
  login({ commit }, userInfo) {
    console.log(auth);
    auth.login(userInfo).then(data => {
      setToken(data.token);
      toast(`Welcome ${data.username}`, 'success');
      commit("LOGIN_USER");
      router.push('/home').catch(err => {
        console.log(err);
      });
    }).catch(err => {
      toast(err, 'error');
    });

  },
  signup(userInfo) {
    auth.signup(userInfo).then(() => {
      this.login(userInfo);
    }).catch(err => {
      toast(err, 'error');
    });
  },
  logout({ commit }) {
    auth.logout().then(() => {
      removeToken();
      commit('LOGOUT_USER');
      router.push('/login').catch(err => {
        console.log(err);
      });
    }).catch(err => {
      console.log(err);
    });
  }
};

const getters = {
  isLoggedIn: state => state.isLoggedIn
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
};
