import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/login'
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/Login')
  },
  {
    path: '/signup',
    name: 'Signup',
    component: () => import('../views/Signup')
  },
  {
    path: '/home',
    name: 'Home',
    component: Home
  },
  {
    path: '/recipe/add',
    component: () => import('../views/Recipe/Add.vue')
  },
  {
    path: '/recipe/:id',
    component: () => import('../views/Recipe/ShowRecipe.vue')
  },
  {
    path: '/cart',
    component: () => import('../views/Cart')
  }
]

const createRouter = () => new VueRouter({
  routes
});

const router = createRouter();

export function resetRouter() {
  const newRouter = createRouter();
  router.matcher = newRouter.matcher; // reset router
}

export default router
