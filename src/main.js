import Vue from 'vue';
import App from './views/App.vue';
import router from './router';
import store from './store';
import './utils/permission';
import Toasted from 'vue-toasted';
import Unicon from 'vue-unicons';
import { uniShoppingCart, uniSignout } from 'vue-unicons/src/icons';

Unicon.add([uniShoppingCart, uniSignout])
Vue.use(Unicon)

Vue.use(Toasted, {
  theme: "toasted-primary",
  duration: 4000,
  singleton: true
})

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
