import router from '../router';
import NProgress from 'nprogress'; // progress bar
import 'nprogress/nprogress.css'; // progress bar style
import { getToken, removeToken } from '@/api/auth'; // get token from localStorage
import { toast, clearToasts } from './toast'; // show toast

NProgress.configure({ showSpinner: false }); // NProgress Configuration

const whiteList = ['/login', '/signup']; // routes doesn't need authorization

router.beforeEach(async(to, from, next) => {
  // start progress bar
  NProgress.start();

  // determine whether the user has logged in
  const hasToken = getToken();

  if (hasToken) {
    if (to.path === '/login') {
      // if is logged in, redirect to the home page
      next('/home');
    } else {
      next();
    }
    NProgress.done();
  } else {
    if (whiteList.indexOf(to.path) !== -1) {
      // if in the free login whitelist, go directly
      next();
    } else {
      // remove token, clear previous toasts, show error toast
      // and go to login page to re-login
      removeToken();
      clearToasts();
      toast('You are not authorized. Please login first', 'error');
      next('/login');
      NProgress.done();
    }
  }
});

router.afterEach(() => {
  // finish progress bar
  NProgress.done();
});
