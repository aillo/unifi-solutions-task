/*
 * Returns max element in array 
 */

export function maxInArray(arr, isObject, compareKey) {
  // Check first argument type if array
  if (!(arr instanceof Array)) {
    throw new Error(`Expected argument type to be an array but found ${typeof arr} instead`);
  }
  
  // eslint-disable-next-line no-extra-boolean-cast
  if (!!isObject) {
    return arr.reduce((result, current) => {
      return Math.max(result, current[compareKey]);
    }, -1);
  } else {
    return arr.reduce((result, current) => Math.max(result, current), -1);
  }

}
