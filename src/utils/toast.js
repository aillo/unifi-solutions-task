import Vue from 'vue';

export function toast(message, type) {
  Vue.toasted.show(message, {
    type: type
  });
}

export function toastError(message) {
  Vue.toasted.error(message);
}

export function clearToasts() {
  Vue.toasted.clear();
}